<!--
The script at the path `docs.sh` in the project's directory generates this
document from certain comment lines in files at the paths

    main.yml

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    #### DOC BLOCK BEGIN

and ends with the exact line

    #### DOC BLOCK END

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one `#` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one `#` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document.
-->

Overview
--------

The Ansible inventory _may_ assign a string to the `domain` variable of a
machine.  Say that a list of domains _selects_ a machine iff the inventory
assigns one of the domains in the list to the `domain` variable of the
machine.

For any list of domains, this Ansible playbook tries to ensure that a unique
instance of each selected KVM guest exists, with parameters as specified in
the Ansible inventory, on some selected KVM host.

Guest creation
--------------

The memory a qemu process requires in order to be able to run a KVM guest
`«g»`, without the risk that the guest tries to access RAM the process does
not provide, is

    qemu(«g») + ram(«g»)

where

*   `ram(«g»)` is the RAM `«g»` has, and

*   `qemu(«g»)` is the memory the qemu process requires in order to run
    `«g»`, excluding `ram(«g»)`.

Thus, the memory a KVM host `«h»` requires in order to be able to run all of
its current KVM guests, without the risk that a guest tries to access RAM its
qemu process does not provide, is

    base(«h») + ∑ (qemu(«g») + ram(«g»))
            «g» ∊ guests(«h»)

where

*   `base(«h»)` is the memory `«h»` requires in order to run all of its
    processes, excluding qemu processes that run KVM guests, and
 
*   `guest(«h»)` is the set of current KVM guests of `«h»`.

Note, the members of `guests(«h»)` need not be currently running -- they are
still potential consumers of memory that must be taken into account.

Thus, the memory a KVM host `«h»` can offer in order to run additional KVM
guests, without offering more memory than the sum of its RAM and a fraction
of its swap, and without the risk that a guest tries to access RAM its qemu
process does not provide, is

    ram(«h») + (swap(«h») * frac(«h»)) - base(«h») - ∑ (qemu(«g») + ram(«g»))
                                                 «g» ∊ guest(«h»)

where

*   `ram(«h«)` is the RAM `«h»` has,

*   `swap(«h»)` is the swap `«h»` has, and

*   `frac(«h»)` is the fraction of `swap(«h«)` `«h»` is prepared to offer its
    guests.

`ram(«h»)`, `swap(«h»)`, `frac(«h»)` and `ram(«g»)` have fixed values for
`«h»` and `«g»`, but `base(«h»)` and `qemu(«g»)` do not.  They can be
measured (if `«g»` is running), but their values change as the host and guest
kernels allocate, reallocate, cache, page and free memory.  At best, we can
estimate reasonable maxima for `base(«h»)` and `qemu(«g»)` and tune the
estimates over time.  Moreover, since all KVM hosts run Alpine Linux (albeit,
different versions perhaps), we can assume that the maximum of `base(«h»)` is
the same for each host `«h»`, and the maximum for `qemu(«g»)` is the same for
each guest `«g»`.  Suppose then that

*   `qemu` is an estimate of the maximum memory a qemu process requires in
    order to run its guest, excluding the RAM its guest has, and

*   `base` is an estimate of the maximum memory a KVM host requires in order
    to run all of its processes, excluding qemu processes that run KVM
    guests.

It follows that the memory a qemu process requires in order to be able to run
a KVM guest `«g»`, without the risk that the guest tries to access RAM the
process does not provide, is approximately

    qemu + ram(«g»)

and the memory a KVM host `«h»` can offer in order to be able to run
additional KVM guests, without offering more memory than the sum of its RAM
and a fraction of its swap, and without the risk that a guest tries to access
RAM its qemu process does not provide, is approximately

    ram(«h») + (swap(«h») * frac(«h»)) - base - ∑ (qemu + ram(«g»))
                                            «g» ∊ guest(«h»)

If a selected KVM guest exists on no selected KVM host then this playbook
tries to schedule some selected host to create the guest.  Loosely speaking,
the playbook schedules the selected host that currently offers the most
memory to create the selected and unscheduled guest that requires the most
memory.  More accurately, this playbook

1.  fails if two or more selected hosts each have a selected guest such that
    the guests have the same name,

2.  fails if some selected host has some selected guest such that the actual
    state of the guest differs from the Ansible inventory parameters for the
    guest,

3.  identifies the selected host that currently offers the most memory,

4.  identifies the selected, non-existent and unscheduled guest that requires
    the most memory,

5.  fails if the identified guest requires more memory than the identified
    host offers,

6.  schedules the identified host to create the identified guest,

7.  subtracts the memory that the identified guest requires from the memory
    that the identified host offers, and

8.  repeats steps 3 to 7 until it fails or schedules all selected and
    non-existent guests for creation.

Note, this heuristic is absurdly naïve.  Firstly, it only considers memory.
Secondly, it can fail even when a schedule is possible.  For example, if

*   selected hosts `«host₁»` and `«host₂»` offer respectively 5GB and 4GB of
    memory, and

*   selected guests `«guest₁»`, `«guest₂»` and `«guest₃»` require
    respectively 4GB, 3GB and 2GB of memory

then the heuristic

*   schedules `«host₁»` (the host that currently offers the most memory) to
    create `«guest₁»` (the unscheduled guest that requires the most memory),
    leaving `«host₁»` with 1GB to offer,

*   schedules `«host₂»` (now the host that currently offers the most memory)
    to create `«guest₂»` (now the unscheduled guest that requires the most
    memory), leaving `«host₂»` with 1GB to offer, and

*   cannot schedule any host to create `«guest₃»`

even though `«host₁»` could have accomodated `«guest₂»` and `«guest₃»` while
`«host₂»` accomodated `«guest₁»`.  I will nonetheless keep to this heuristic
for several reasons.  Firstly, this playbook is intended as a makeshift tool.
Secondly, I suspect that a correct solution would be NP-complete!  And
finally, why reinvent a scheduler that probably already exists in OpenStack
or Kubernetes!!

Usage
-----

For each list of domains `«domain₁»` ... `«domainₙ»`, the call

    play kvm_guest @ «domain₁» ... «domainₙ»

in the root of the Ansible directory tree executes this playbook on each KVM
host and KVM guest selected by the list.

Each selected KVM host _may_ have the Ansible inventory parameter

    swap_fraction: «swap_fraction»

where

*   the positive integer `«swap_fraction»` in the range 0 to 1024 inclusive
    is the bytes per KiB of the host's swap that the host will offer to its
    guests.

Each selected KVM guest _must_ have the Ansible inventory parameters 

    socket_count: «socket_count»
    core_count: «core_count»
    thread_count: «thread_count»
    ram: «ram»
    backing_path: «path»

where

*   the positive integer `«socket_count»` is the number of CPU sockets the
    guest should have,

*   the positive integer `«core_count»` is the number of cores per socket the
    guest should have,

*   the positive integer `«thread_count»` is the number of threads per core
    the guest should have,

*   the positive integer `«ram»` is the MiB of RAM the guest should have, and

*   the string `«backing_path»` is a file-system path to the backing storage
    volume the guest should have.

The guest _may_ also have the Ansible inventory parameters

    connections:
    - «bridge₁»[,«ipv4_address₁»]
    - ⋮
    - «bridgeₙ»[,«ipv4_addressₙ»]
    state: «state»
    autostart: «autostart»

where

*   the string `«bridgeᵤ»` is the name of an Ethernet bridge the guest should
    be connected to (NOT YET USED),

*   the optional string `«ipv4_addressᵤ»` is an IPv4 address the guest should
    request from the DHCP server on the bridge `«bridgeᵤ»` by means of a
    "magic" MAC address (see the `kvm_tool get mac address` command call of
    https://levigo.de/stash/projects/LC/repos/kvm.kvm_tool) (NOT YET USED),

*   the string `«state»` is the state the guest should be in, one of `shut
    off`, `running` or `paused` (the guest should be in the state `shut off`
    if the parameter is not set), and

*   the string `«autostart»` is `on` if the guest should be marked for
    autostart and `off` if the guest should not be marked for autostart (the
    guest should not be marked for autostart if the parameter is not set).

docs.sh
-------

The script at the path `docs.sh` in the project's directory generates this
document from certain comment lines in files at the paths

    main.yml

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    #### DOC BLOCK BEGIN

and ends with the exact line

    #### DOC BLOCK END

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one `#` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one `#` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document.
