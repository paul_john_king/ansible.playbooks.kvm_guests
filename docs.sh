#/bin/sh

set -e ;
set -u ;

_echo_stdin(){

	if test ${#} -ne 0 ;
	then
		echo "Usage: _echo_stdin" ;
		return 2 ;
	fi ;

	local _line ;

	while IFS= read -r _line ;
	do
		echo "${_line}" ;
	done ;

	return 0 ;

}

_extract_block_from_file_at_path(){

	if test ${#} -ne 3 ;
	then
		echo "Usage: _extract_block_from_file_at_path «block_begin» «block_end» «file_path»" ;
		return 2 ;
	fi ;
	local _BLOCK_BEGIN="${1}"
	local _BLOCK_END="${2}"
	local _FILE_PATH="${3}" ;
	shift 3 ;

	local _HASH_SPACE="# " ;
	local _HASH_TAB="#	" ;
	local _HASH="#" ;

	local _write_line ;
	local _line ;
	local _prefix ;

	_write_line=false ;
	while IFS= read -r _line ;
	do
		_prefix="${_line%%[^ 	]*}" ;
		_line="${_line#${_prefix}}" ;
		case "${_line}" in
			"${_BLOCK_BEGIN}")
				_write_line=true ;
			;;
			"${_BLOCK_END}")
				_write_line=false ;
			;;
			"${_HASH_SPACE}"*)
				${_write_line} && echo "${_line#${_HASH_SPACE}}" ;
			;;
			"${_HASH_TAB}"*)
				${_write_line} && echo "${_line#${_HASH_TAB}}" ;
			;;
			"${_HASH}"*)
				${_write_line} && echo "" ;
			;;
		esac ;
	done < "${_FILE_PATH}" ;

	return 0 ;

} ;

_underline(){

	if test ${#} -ne 1 ;
	then
		echo "Usage: _underline «string»" ;
		return 2 ;
	fi ;
	local _STRING="${1}"
	shift 1 ;

	printf "%s\n%.*s\n" "${_STRING}" ${#_STRING} "--------------------------------------------------------------------------------" ;

	return 0 ;

} ;

_main(){

	local _HOME="${0%/*}" ;
	local _NAME="${0##*/}" ;

	if test ${#} -lt 1 ;
	then
		echo "Usage: ${_NAME} «target_path» [«source_path_1» ... «source_path_n»] " ;
		return 2 ;
	fi ;
	local _TARGET_PATH="${1}"
	shift 1 ;

	local _BLOCK_BEGIN="#### DOC BLOCK BEGIN"
	local _BLOCK_END="#### DOC BLOCK END"
	local _WARNING="\
The script at the path \`${_NAME}\` in the project's directory generates this
document from certain comment lines in files at the paths

$(
	for _file in "${@}" ;
	do
		echo "    ${_file}" ;
	done ;
)

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    ${_BLOCK_BEGIN}

and ends with the exact line

    ${_BLOCK_END}

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one \`#\` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one \`#\` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document."

	local _file ;

	(
		cd "${_HOME}" ;

		exec 1> "${_TARGET_PATH}" ;

		_echo_stdin <<- __END_OF_STDIN__
<!--
${_WARNING}
-->

		__END_OF_STDIN__

		for _file in "${@}" ;
		do
			_extract_block_from_file_at_path "${_BLOCK_BEGIN}" "${_BLOCK_END}" "${_file}" ;
		done ;

		_echo_stdin <<- __END_OF_STDIN__
$(_underline "${_NAME}")

${_WARNING}
		__END_OF_STDIN__

		return 0 ;

	) ;

	return 0 ;

} ;

_main "README.md" "main.yml" ;
